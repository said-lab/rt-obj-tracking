# Stepping towards real-time detection and tracking of apples using deep learning

This is the code to our research 'Stepping towards real-time detection and tracking of apples using deep learning'. We provide scripts and tutorials for applying multi-object tracking and segmentation (MOTS) on an apple orchard by using the [TrackR-CNN][TrackR-CNN_MOTS] algorithm. 

To use the **PointTrack** algorithm instead of **TrackR-CNN**, go to the [PointTrack](algorithms/PointTrack/) folder for instructions.

## Video of results

![](visualizations/test_8_wearable_sensor.gif)

A video of the results of TrackR-CNN on our test set can be found on [Youtube][Youtube_link].

## Dataset

We created our own dataset called **APPLE_MOTS** related to the [KITTI_MOTS dataset][Kitti_MOTS] of [TrackR-CNN][TrackR-CNN_MOTS] (persons and cars). 
Images and annotations of APPLE_MOTS can be downloaded [here][data].  Some example pictures of the 9 datasets:

![](visualizations/dataset_1.png) ![](visualizations/dataset_2.png) ![](visualizations/dataset_3.png)
![](visualizations/dataset_4.png) ![](visualizations/dataset_5.png) ![](visualizations/dataset_6.png)
![](visualizations/dataset_7.png) ![](visualizations/dataset_8.png) ![](visualizations/dataset_9.png)

## Custom dataset
If you want to apply TrackR-CNN to your own custom dataset. Make sure that the following requirements are fulfilled:
- Create frames of your data (generated output consists of right format 00001, 00002 etc.)
    - use `pre_processing/compress_to_frames` for videos to frames 
    - use `pre_processing/JPG_to_PNG` for frames (jpg) to frames (png)
- Create instance annotations using the [CVAT][CVAT_site] annotation tool.
    - Create [tracks][CVAT_tracks] of all objects 
    - Export format as MOTS PNG
    
Hence, create a dataset that looks similar like the [KITTI_MOTS dataset][Kitti_MOTS].
For more information about using a custom dataset consult the PowerPoint [annotation_procedure][PP]

## Installation
### Hardware
TrackR-CNN is tested on a high performance computer (HPC) with the following specs:
- NVIDIA Titan RTX videocard
- 64 GB RAM memory
- Intel® Core™ i9-10940X CPU @ 3.30GHz × 28
- Ubuntu 20.04.1 LTS
### Requirements
- Install [Anaconda][CONDA]
- Create a new environment
    - `conda create --name myenv python=3.6.7`
    - `conda activate myenv`
- Clone this repository 
    - `git clone https://git.wur.nl/said-lab/rt-obj-tracking.git`
    - `cd rt-obj-tracking`
- Install requirements
    - `pip install -r requirements.txt`
- Move to algorithms/TrackR-CNN and create folders
    - `cd algorithms/TrackR-CNN`
    - `mkdir forwarded models summaries logs data`
- Download the data and add it to `data` and create the following structure:
```
data/
- APPLE_MOTS/
-- train/
--- images/
---- 0000/
----- 000000.png
----- 000001.png
---- 0001/
----- 000000.png
----- 000001.png
---- ...
--- instances/
---- 0000/
----- 000000.png
----- 000001.png
----- ...
---- 0001/
----- 000000.png
----- 000001.png
---- ...
```

## Pretrained models
If you want to use the pretrained models on APPLE_MOTS, download it [here][pretrained]. Add them to the `models` folder.

## Training
When using pretrained models, this step can be omitted. 

To train a model, run `main.py` with a corresponding configuration file e.g. `python main.py configs/3dconv8_24` (`cd algorithms/TrackR-CNN`). Seven models (3d_conv) use two 3D conv layers where model eight (lstm) uses two stacked lstm layers. The description behind the model corresponds to respectively the batch size and number of epochs. Note that the number of epochs in the configuration files are calculated by the total number of epochs / batch size. Because for every epoch all possible batches of e.g. 8 consecutive frames are processed. 

### APPLE_MOTS

There are three possibilities to train TrackR-CNN on APPLE_MOTS:
1. Train APPLE_MOTS from scratch (only pretrained on COCO and Mapillary).
- Change in a config file:
    - model name e.g. `"model": "conv3d_sep2"` to `"model": "new_model"`
    - locate "load init" to your `models` folder, e.g. `"load_init": "/home/saidlab/Thesis_Stefan_de_Jong/rt-obj-tracking/algorithms/TrackR-CNN/models/converted"`
    - locate "KITTI_segtrack_data_dir" to your `train` folder e.g. `"/home/saidlab/Thesis_Stefan_de_Jong/rt-obj-tracking/algorithms/TrackR-CNN/data/APPLE_MOTS/train/"`
    - Set your batch size e.g. `"batch_size": 4`
    - Set number of epochs e.g. `"num_epochs": 4`
- run `python main.py configs/A_CONFIG_FILE`

2. Train APPLE_MOTS from KITTI MOTS checkpoint (fine-tuned on KITTI MOTS and pretrained on COCO and Mapillary) 
- Change in a config file:
    - model name e.g. `"model": "conv3d_sep2"` to `"model": "new_model"`
    - locate "load init" to your `models` folder, e.g. `"load_init": "/home/saidlab/Thesis_Stefan_de_Jong/rt-obj-tracking/algorithms/TrackR-CNN/models/conv3d_sep2/conv3d_sep2-00000005"`
    - locate "KITTI_segtrack_data_dir" to your `train` folder e.g. `"/home/saidlab/Thesis_Stefan_de_Jong/rt-obj-tracking/algorithms/TrackR-CNN/data/APPLE_MOTS/train/"`
    - Set your batch size e.g. `"batch_size": 4`
    - Set number of epochs e.g. `"num_epochs": 4`
- run `python main.py configs/A_CONFIG_FILE`

3. Train APPLE_MOTS on e.g. `3dconv8_24` by using more epochs (in this case 1 epoch more):
- Change in a config file:
    - locate "load init" to your `models` folder, e.g. `"load_init": "/home/saidlab/Thesis_Stefan_de_Jong/rt-obj-tracking/algorithms/TrackR-CNN/models/3dconv8_24/3dconv8_24-00000003"`
    - Set number of epochs `"num_epochs": 4` should be higher than 3dconv8_24-0000000**3** otherwise it doesn't train
    - locate "KITTI_segtrack_data_dir" to your `train` folder e.g. `"/home/saidlab/Thesis_Stefan_de_Jong/rt-obj-tracking/algorithms/TrackR-CNN/data/APPLE_MOTS/train/"`
- run `python main.py configs/A_CONFIG_FILE`
### Custom dataset
1. Train custom dataset from KITTI MOTS checkpoint (_option 1_) or scratch (_option 2_).
- Change in a config file:
    - model name e.g. `"model": "conv3d_sep2"` to `"model": "new_model"`
    - locate "load init" to your `models` folder, e.g.:
        - Option 1 `"load_init": "/home/saidlab/Thesis_Stefan_de_Jong/rt-obj-tracking/algorithms/TrackR-CNN/models/conv3d_sep2/conv3d_sep2-00000005"` 
        - Option 2 `"load_init": "/home/saidlab/Thesis_Stefan_de_Jong/rt-obj-tracking/algorithms/TrackR-CNN/models/converted"` 
    - locate "KITTI_segtrack_data_dir" to your `train` folder e.g. `"/home/saidlab/Thesis_Stefan_de_Jong/rt-obj-tracking/algorithms/TrackR-CNN/data/APPLE_MOTS/train/"`
    - Set your batch size e.g. `"batch_size": 4`
    - Set number of epochs e.g. `"num_epochs": 6`
    - Set `"input_size_train"`  and `"input_size_val"` to right resolutions. 
- Calculate maximum number of detections in your dataset
    - Go to home folder e.g. (`cd rt-obj-tracking`)
    - Check if config.py is set to right location of datasets
    - run `python main.py`
    - Change `N_MAX_DETECTIONS` variable in `rt-obj-tracking/algorithms/TrackR-CNN/datasets/KITTI/segtrack/KITTI_segtrack.py` to printed output of terminal (currently set to 200)
- run `python main.py configs/A_CONFIG_FILE`

## Tracking
Download the pre-trained models or use your own trained model to forward and track the objects. In this step the network is evaluated on the given input sequences and it creates a tracking output. The `load_epoch_no` should correspond to the number of epochs the model is trained on. The json string overwrites the settings in the config file. The `video_tags_to_load` corresponds to the sequences you want to track (in this case the whole dataset). NOTE: make sure that you add the testing datasets to the train dataset folder if you want to apply it on all sequences.    
- Obtain model predictions:
```
python main.py configs/3d_conv8_24 "{\"task\":\"forward_tracking\",\"dataset\":\"KITTI_segtrack_feed\",\"load_epoch_no\":3,\"batch_size\":5,\"export_detections\":true,\"do_tracking\":false,\"video_tags_to_load\":[\"0000\",\"0001\",\"0002\",\"0003\",\"0004\",\"0005\",\"0006\",\"0007\",\"0008\"]}"
```
- Link the predictions over time and visualize the results:
```
python main.py configs/3dconv8_24 "{\"build_networks\":false,\"import_detections\":true,\"task\":\"forward_tracking\",\"dataset\":\"KITTI_segtrack_feed\",\"do_tracking\":true,\"visualize_detections\":false,\"visualize_tracks\":true,\"load_epoch_no\":3,\"video_tags_to_load\":[\"0000\",\"0001\",\"0002\",\"0003\",\"0004\",\"0005\",\"0006\",\"0007\",\"0008\"]}"
```
The output can be found in the `forwarded` folder. The visuals can be found in the `vis` map.

## Tuning
Tuning will be used to find the most suitable parameters to detect and track your objects. A random search approach is been applied on the training dataset (`train.seqmap`) and will be validated on your validation set (`val.seqmap`). 
Both the training and validation sequences should be stored in the `train` folder ("KITTI_segtrack_data_dir"). In order to specify the sequences for training and testing please follow this procedure:
- Go to TrackR-CNN/mots_tools/mots_eval/
- Open train.seqmap and val.seqmap
- Add the sequences for training (train.seqmap) and validation (val.seqmap) according to the following format:
    - `<seq_id> empty <start_frame> <end_frame>`

## Visualization
For changing the info (class) that's displayed in the detections mask, please change the `category_name` in `TrackR-CNN/forwarding/RecurrentDetectionForwarder.py.visualize_detections`:
```
if class_ == 1:
      category_name = "Car"
    elif class_ == 2:
      category_name = "Pedestrian"
    else:
      category_name = "Ignore"
      color = (0.7, 0.7, 0.7)
```
Hence, `Car` could be changed by `Apple` to display e.g. _Apple: 1_ instead of _Car: 1_ for the first detection mask.

## References
The code from the folder `algorithms/TrackR-CNN` and parts of the README are used from [TrackR-CNN][TrackR-CNN_repo].
The annotation tool of [CVAT][CVAT_site] was used to create the annotations. 

## Citation
When using this code please cite:


## Future research
### Optical flow
For future research it can be of interest to implement optical flow within TrackR-CNN. Two data loaders are used during optical flow implementation in TrackR-CNN: `load_optical_flow` & `open_flow_png_file`. A predefined file format is used to calculate minimal flow values in x and y direction. E.g. _000194_x_minimal5.png_ describes the flow in x direction for frame 194 where the minimum flow was 5. _000369_y_minimal-11.png_ is the flow in y direction for frame 369 with a minimal flow of -11. The function `open_flow_png_file` deals with opening the png files and adding back the minimal values as given in the filename. So, when you have some flow files (computed e.g. using Pwcnet), you must first compute the minimum value per file, subtract it from the values for all pixels in that file and then store the result as unsigned 16bit integer .png file. (or you could rewrite `open_flow_png_file` to fit your format). Then `load_optical_flow` can be used for loading the information to establish tracking. Two example optical flow images can be downloaded [here][Opticalflow].
### Tensorflow Object Detection API
For using a simple bounding box counting method the Tensorflow Object Detection API in combination with a counting line (see image) can be applied to create a real time counter. The necessary Tensorflow annotation format (.tfrecord) can be downloaded [here][Tensorflow].



![](visualizations/Count_Tensorflow.jpg)



## Contact
For questions related to the code please contact stefan.dejong@wur.nl or create an issue. 

[TrackR-CNN_MOTS]: https://www.vision.rwth-aachen.de/media/papers/mots-multi-object-tracking-and-segmentation/MOTS.pdf
[TrackR-CNN_repo]: https://github.com/VisualComputingInstitute/TrackR-CNN
[Kitti_MOTS]: https://www.vision.rwth-aachen.de/page/mots
[Youtube_link]: https://youtu.be/6pNhdK-T_e4 
[data]: https://drive.google.com/file/d/1WbNRwyAhLY4afib7oT1VceeK1fNwup7p/view?usp=sharing
[pretrained]: https://drive.google.com/file/d/1hYXeg2-bnR2BltPituq_3bYaT830Ji4i/view?usp=sharing
[Tensorflow]: https://drive.google.com/file/d/14giotFJ_dFj5Dl3NHkZCC75uwWh5FV82/view?usp=sharing
[CVAT_site]: https://github.com/openvinotoolkit/cvat
[CVAT_tracks]: https://cvat.org/documentation/user_guide.html#track-mode-basics
[PP]: https://git.wur.nl/said-lab/rt-obj-tracking/-/tree/master/pre_processing
[CONDA]: https://docs.anaconda.com/anaconda/install/
[Opticalflow]: https://drive.google.com/file/d/1gOPGxalyi7vNL6GSardhYFxZQw6VNoXo/view?usp=sharing
