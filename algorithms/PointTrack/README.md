# Realtime Apples Detection and Tracking using UAVs

This codebase implements [PointTrack](https://github.com/detectRecog/PointTrack), a highly effective framework for multi-object tracking and segmentation (MOTS) described in: 

```
@inproceedings{xu2020Segment,
  title={Segment as Points for Efficient Online Multi-Object Tracking and Segmentation},
  author={Xu, Zhenbo and Zhang, Wei and Tan, Xiao and Yang, Wei and Huang, Huan and Wen, Shilei and Ding, Errui and Huang, Liusheng},
  booktitle={Proceedings of the European Conference on Computer Vision (ECCV)},
  year={2020}
}
```



**PointTrack presents a new learning strategy for pixel-wise feature learning on the 2D image plane, which has proven to be effective for instance association.**

The network architecture adopts [SpatialEmbedding](https://github.com/davyneven/SpatialEmbeddings) as the segmentation sub-network.
  

## Getting started

This codebase showcases the proposed framework named PointTrack for MOTS using the a MOTS annotated dataset. 

### Prerequisites
Dependencies: 
- Pytorch 1.3.1 (and others), please set up an virtual env and run:
```
$ pip install -r requirements.txt
```
- Python 3.6 (or higher)
- [KITTI Images](http://www.cvlibs.net/download.php?file=data_tracking_image_2.zip) + [Annotations](https://www.vision.rwth-aachen.de/media/resource_files/instances.zip)

Note that the scripts for evaluation is included in this repo. After images and instances (annotations) are downloaded, put them under **kittiRoot** and change the path in **repoRoot**/config.py accordingly. 
The structure under **kittiRoot** should looks like:

```
kittiRoot
│   images -> training/image_02/ 
│   instances
│   │    0000
│   │    0001
│   │    ...
│   training
│   │   image_02
│   │   │    0000
│   │   │    0001
│   │   │    ...  
│   testing
│   │   image_02
│   │   │    0000
│   │   │    0001
│   │   │    ... 
```

## Config.py file

Change the file directories accordingly.
`kittiRoot` is the directory where the dataset is stored.

### Pretrained weights
Fine-tuned models on KITTI MOTS (for cars):
- SpatialEmbedding for cars.
- PointTrack for cars.
You can download them via [Baidu Disk](https://pan.baidu.com/s/1Mk9JWNcM1W08EAjhyq0yLA) or [Google Drive](https://drive.google.com/open?id=14Hn4ZztfjGUYEjVd-9FRNB5a-CtBkPXc).
- testset segs can be found in the folder 'testset_segs'.

For Apples, models will be uploaded soon.
The testset segs are found in the folder 'apple_testset_segs'

## Train a dataset from scratch
To train a dataset from scratch, follow instructions from the master branch of this repository on how to label a custom dataset.
Next, order the dataset folders in accordance to the `kittiRoot` structure above.
Next, create a new environment and install all the dependencies.

The steps when training from scratch is as follows:
1. Train the instance segmentation model for detection (SpatialEmbedding in this case). The output is a pth file called **checkpoint.pth**
2. Train the PointTrack model, the model that will do the instance associations. The output is also a pth file called **checkpoint.pth**, not to be confused with the pth file from SpatialEmbedding.
3. Evaluating the models, first by generating the instance segmentation results (forwarding), then running tracking results.


## Training of SpatialEmbedding

An older way of training SpatialEmbedding with KITTI MOTS needs KITTI object detection left color images as well as the KINS annotations.
However, that is not necessary anymore. It can be done by copying MOTS ground truth annotations to a folder named KINS inside the training and testing folder of kittiRoot.

A folder structure of kittiRoot needs to be structured in the following:

```
kittiRoot
│   images -> training/image_02/ 
│   instances
│   │    0000
│   │    0001
│   │    ...
│   training
│   │   image_02
│   │   │    0000
│   │   │    0001
│   │   │    ...  
│   │   KINS
│   │   │   0000
│   │   │   0001
│   │   │   ...
│   testing
│   │   image_02
│   │   │    0000
│   │   │    0001
│   │   │    ... 
│   │   KINS
│   │   │   0000
│   │   │   0001
│   │   │   ...
```

Each of the KINS subfolders should be filled with the instance PNGs of your dataset.

For the training of SpatialEmbedding, we follow the original training setting of [SpatialEmbedding](https://github.com/davyneven/SpatialEmbeddings). 

1.This step selects valid frames that contains objects of the class; since there could be frames that contain no masks of the object.
 ```
$ python -u datasets/MOTSImageSelect.py
 ```

2.Before generating the crops, make sure that the crops sizes are correct in the file/code. Then do the following:
```
$ python -u utils/generate_crops.py
```
After this step, crops are saved under **kittiRoot**/crop_KINS.

3.Afterwards start training on crops: 
```
$ python -u train_SE.py car_finetune_SE_crop
```
You can change desired parameters inside the config file.

4.Afterwards start finetuning on KITTI MOTS with BN fixed. SpatialEmbeddings trained the finetuning with a crop size 2x larger than the first training.
This can be changed in the file from step **2**

Create a file on the root with the name **resume_SE**, then copy the **checkpoint.pth** file into it.

Then run the following command:
```
$ python -u train_SE.py car_finetune_SE_mots
```
The finetuning will be saved to a different folder.

## Training of PointTrack
The training procedure of instance association is as follows.

1.To generate the segmentation result on the validation set as the instruction of the first step in Testing.
Make sure the file **datasets/KittiMOTSDataset.py** and seqmaps in **datasets/mots_tools/mots_eval/** has the correct dataset configuration.

Then, copy the checkpoint.pth file from the finetuning folder to the **tuned** folder in `rootDir`.

Finally run the segmentation result (forwarding):

```
$ python -u test_mots_se.py car_test_se_to_save
```

2.To generate the instance DB from videos:
```
$ python -u datasets/MOTSInstanceMaskPool.py
```

3.Change the line in **car_finetune_tracking.py** which loads weights to the default path as follows:
```
checkpoint_path='./car_finetune_tracking/checkpoint.pth'
```

4.Afterwards start training:
```
$ python -u train_tracker_with_val.py car_finetune_tracking
```
The best tracker on the validation set will be saved under the folder specified in **repoRoot**/config_mots/car_finetune_tracking.py.


## Testing

You can download pretrained models from the above links. Save these weight file under **repoRoot**/pointTrack_weights.

1.To generate the instance segmentation results:

```
$ python -u test_mots_se.py car_test_se_to_save
```
The segmentation result will be saved according to the config file **repoRoot**/config_mots/car_test_se_to_save.py.

2.To test PointTrack on the instance segmentation results:
```
$ python -u test_tracking.py car_test_tracking_val
```

## Contact
If you find problems in the code, please open an issue, or contact Hilmy (hilmy.baja@wur.nl)






