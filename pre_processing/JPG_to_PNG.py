"""This module transforms .jpg images to .png while using the
output image format according to 000000, 000001 etc."""

#Import libraries
from PIL import Image
import os

in_path='/media/stefan/Elements/Backup/Master_thesis/Data/Current_data/Validation/Human_Centered_Device'
folders=os.listdir(in_path)
#Convert jpg images to png
for folder in folders:
    count=0
    files=sorted(os.listdir(os.path.join(in_path, folder)))
    for file in files:
        print(file)
        print(os.path.join(in_path, folder,file))
        im=Image.open(os.path.join(in_path, folder,file))
        newvar=str(count).zfill(6)
        count+=1
        im.save(os.path.join(in_path, folder)+ '/'+ newvar + '.png')





    



