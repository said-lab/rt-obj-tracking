"""This file runs the main scripts for analysing the dataset"""
import config 
from analysis.n_max_detections import max_detections
from analysis.overview_datasets import overview

# Calculate the maximum number of detections
print('The maximum number of detections are:', max_detections(config.TRAIN_DIR, config.TEST_DIR))

# Create an overview of the dataset
print(overview(config.TRAIN_DIR, config.TEST_DIR))