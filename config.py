import os


global ROOT_DIR, DATA_DIR, TRAIN_DIR, TEST_DIR
    
ROOT_DIR    = os.path.dirname(os.path.abspath(__file__))

DATA_DIR = os.path.join(ROOT_DIR, 'algorithms/TrackR-CNN/data/APPLE_MOTS')
TRAIN_DIR = os.path.join(ROOT_DIR, 'algorithms/TrackR-CNN/data/APPLE_MOTS/train')
TEST_DIR = os.path.join(ROOT_DIR, 'algorithms/TrackR-CNN/data/APPLE_MOTS/testing')
